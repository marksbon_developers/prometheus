-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 10:15 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ihrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_dashboard_items`
--

CREATE TABLE `access_dashboard_items` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `icon` varchar(45) NOT NULL,
  `link` varchar(255) NOT NULL,
  `bg_color` varchar(45) NOT NULL,
  `priviliges` text NOT NULL,
  `system_category` varchar(45) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_password_reset_requests`
--

CREATE TABLE `access_password_reset_requests` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `password_token` varchar(100) NOT NULL,
  `token_expiry_date` datetime NOT NULL,
  `user_agent_info` varchar(255) NOT NULL,
  `status` enum('PENDING','COMPLETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_privileges_group`
--

CREATE TABLE `access_roles_privileges_group` (
  `id` bigint(20) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `roles` text NOT NULL,
  `privileges` text NOT NULL,
  `description` text NOT NULL,
  `login_url` varchar(45) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_privileges_user`
--

CREATE TABLE `access_roles_privileges_user` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `custom_roles` text NOT NULL,
  `custom_privileges` text NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_users`
--

CREATE TABLE `access_users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `default_passwd` varchar(100) NOT NULL,
  `temp_employee_id` varchar(100) NOT NULL,
  `biodata_id` bigint(20) NOT NULL,
  `profile_pic_id` bigint(20) NOT NULL,
  `first_login` tinyint(4) NOT NULL,
  `login_attempt` tinyint(4) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED','') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(11) NOT NULL,
  `award_name` varchar(255) NOT NULL,
  `award_description` varchar(255) NOT NULL,
  `date_of_award` date NOT NULL,
  `file_upload_id` bigint(20) NOT NULL,
  `benefits` varchar(255) NOT NULL,
  `type` enum('OPEN','CLOSED') NOT NULL,
  `frequency` varchar(45) NOT NULL COMMENT 'Monthly,Yearly,One-time',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `award_nominees`
--

CREATE TABLE `award_nominees` (
  `id` bigint(20) NOT NULL,
  `award_id` int(11) NOT NULL,
  `nominee_id` bigint(20) NOT NULL,
  `nominated_by` bigint(20) NOT NULL,
  `nominee_type` enum('EMPLOYEE','DEPARTMENT') NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `award_winners`
--

CREATE TABLE `award_winners` (
  `id` bigint(20) NOT NULL,
  `nominee_rating_id` bigint(20) NOT NULL,
  `position` varchar(45) NOT NULL,
  `date_of_award` date NOT NULL,
  `awarded_by` bigint(20) NOT NULL,
  `award_file_id` bigint(20) NOT NULL,
  `confirmed_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blobs_upload`
--

CREATE TABLE `blobs_upload` (
  `id` bigint(20) NOT NULL,
  `blob_content` longblob NOT NULL,
  `mime_type` varchar(45) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_assets`
--

CREATE TABLE `company_assets` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `asset_type_id` bigint(20) NOT NULL,
  `asset_name` varchar(255) NOT NULL,
  `asset_description` text NOT NULL,
  `asset_condition` varchar(45) NOT NULL,
  `date_acquired` date NOT NULL,
  `acquired_by` bigint(20) NOT NULL,
  `expiry` date NOT NULL,
  `location` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_assets_category`
--

CREATE TABLE `company_assets_category` (
  `id` bigint(20) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_description` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tangible,Intangible,Operating,Non-Operating,Current,Fixed or Non-Current';

-- --------------------------------------------------------

--
-- Table structure for table `company_assets_type`
--

CREATE TABLE `company_assets_type` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Cash and cash equivalents\r\nInventory\r\nInvestments\r\nPPE (Property, Plant, and Equipment)\r\nLand\r\nBuildings\r\nVehicles\r\nFurniture\r\nPatents\r\nStock\r\nEquipment',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `asset_category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_awards_certificates_won`
--

CREATE TABLE `company_awards_certificates_won` (
  `id` int(11) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('AWARD','CERTIFICATE') NOT NULL,
  `date_acquired` date NOT NULL,
  `expiry_date` date NOT NULL,
  `received_by` bigint(20) NOT NULL COMMENT 'Employee who received or handled the award',
  `file_upload_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_award_rating_criteria`
--

CREATE TABLE `company_award_rating_criteria` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_basic_details`
--

CREATE TABLE `company_basic_details` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `history` text NOT NULL,
  `product_description` text NOT NULL,
  `service_description` text NOT NULL,
  `expansion_growth` text NOT NULL,
  `industry_information` text NOT NULL,
  `handbook_file` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_branches`
--

CREATE TABLE `company_branches` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `Parentid` bigint(20) NOT NULL,
  `primary_tel` varchar(45) NOT NULL,
  `secondary_tel` varchar(45) NOT NULL,
  `branch_head` bigint(20) NOT NULL,
  `exact_location` varchar(45) NOT NULL,
  `region` varchar(45) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_branch_departments`
--

CREATE TABLE `company_branch_departments` (
  `id` tinyint(4) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_branch_department_heads`
--

CREATE TABLE `company_branch_department_heads` (
  `id` bigint(20) NOT NULL,
  `department_id` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `hod_biodata_id` bigint(20) NOT NULL,
  `appointed_by` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_appointed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_branch_positions`
--

CREATE TABLE `company_branch_positions` (
  `id` int(11) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `department_id` tinyint(4) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_business_details`
--

CREATE TABLE `company_business_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_established` date NOT NULL,
  `tin_number` varchar(45) NOT NULL,
  `gps_location` text NOT NULL,
  `residence_address` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `primary_tel` varchar(45) NOT NULL,
  `secondary_tel` varchar(45) NOT NULL,
  `fax` varchar(45) NOT NULL,
  `alternative_tel` varchar(45) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `primary_email` varchar(255) NOT NULL,
  `secondary_email` varchar(255) NOT NULL,
  `mission` varchar(255) NOT NULL,
  `vision` varchar(255) NOT NULL,
  `profile_summary` text NOT NULL,
  `logo` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_clients_portfolio`
--

CREATE TABLE `company_clients_portfolio` (
  `id` int(11) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `services_ordered` varchar(255) NOT NULL,
  `evidence_reference` varchar(255) NOT NULL,
  `service_period` varchar(50) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `testimonials` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_targets`
--

CREATE TABLE `company_targets` (
  `id` int(11) NOT NULL,
  `target_category_id` int(11) NOT NULL,
  `objective` varchar(255) NOT NULL,
  `expectation` varchar(45) NOT NULL,
  `start_period` date NOT NULL,
  `end_period` date NOT NULL,
  `ranking` double NOT NULL,
  `remarks` varchar(45) NOT NULL,
  `status` enum('POOR','FAIR','SATISFACTORY','GOOD','EXCELLENT') NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rated_by` bigint(20) NOT NULL,
  `rated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_target_categories`
--

CREATE TABLE `company_target_categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name_of_country` varchar(255) NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_benefits`
--

CREATE TABLE `hr_benefits` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_department_info`
--

CREATE TABLE `hr_department_info` (
  `id` bigint(20) NOT NULL,
  `Parentid` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_biodata`
--

CREATE TABLE `hr_employee_biodata` (
  `id` bigint(20) NOT NULL,
  `title` varchar(5) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `marital_status` enum('SINGLE','MARRIED','DIVORCED','WIDOWED') NOT NULL,
  `curriculum_vitae` bigint(20) NOT NULL,
  `application_letter` bigint(20) NOT NULL,
  `primary_tel` varchar(45) NOT NULL,
  `secondary_tel` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alternate_email` varchar(100) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `residence_address` varchar(255) NOT NULL,
  `city` varchar(45) NOT NULL,
  `nationality` int(11) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `id_card_type` enum('NATION ID','VOTER''S ID','STUDENT''S ID','PASSPORT','DRIVER''S LICENSE','NHIS') NOT NULL,
  `id_card_no` varchar(45) NOT NULL,
  `id_card_issue_date` date NOT NULL,
  `id_card_expiry_date` date NOT NULL,
  `social_security_no` varchar(45) NOT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `bank_branch` varchar(50) DEFAULT NULL,
  `account_no` varchar(45) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_dependents`
--

CREATE TABLE `hr_employee_dependents` (
  `id` bigint(20) NOT NULL,
  `biodata_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `relationship` varchar(45) NOT NULL,
  `primary_tel` varchar(45) NOT NULL,
  `secondary_tel` varchar(45) NOT NULL,
  `residence` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_job_details`
--

CREATE TABLE `hr_employee_job_details` (
  `id` bigint(20) NOT NULL,
  `staff_id` varchar(45) NOT NULL,
  `employment_date` date NOT NULL,
  `biodata_id` bigint(20) NOT NULL,
  `employment_type` enum('FULL TIME','PART TIME','CONTRACT') NOT NULL,
  `job_title` varchar(50) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `negotiated_salary` double NOT NULL,
  `employement_channel` varchar(45) NOT NULL,
  `employement_status` enum('ACTIVE','SUSPENDED','TERMINATED','DECEASED','RESIGNED') NOT NULL,
  `work_phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_leave_request`
--

CREATE TABLE `hr_employee_leave_request` (
  `id` bigint(20) NOT NULL,
  `employee_biodata_id` bigint(20) NOT NULL,
  `leave_policy_id` bigint(20) NOT NULL,
  `availabe_days` int(11) NOT NULL,
  `policy_assigned_date` datetime NOT NULL,
  `date_requested` datetime NOT NULL,
  `reason` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `return_date` date NOT NULL,
  `approval_start_date` date NOT NULL,
  `approval_return_date` date NOT NULL,
  `status` enum('PENDING','APPROVED','DECLINED') NOT NULL,
  `rejection_note` varchar(255) NOT NULL,
  `approver_biodata_id` bigint(20) NOT NULL,
  `approval_response_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_schools_attended`
--

CREATE TABLE `hr_employee_schools_attended` (
  `id` bigint(20) NOT NULL,
  `name_of_school` varchar(255) NOT NULL,
  `degree` varchar(255) NOT NULL,
  `field_of_study` varchar(255) NOT NULL,
  `year_completed` year(4) NOT NULL,
  `additional_notes` varchar(255) NOT NULL,
  `interests` varchar(255) NOT NULL,
  `biodata_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_work_experience`
--

CREATE TABLE `hr_employee_work_experience` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `job_title` varchar(100) NOT NULL,
  `period` varchar(45) NOT NULL,
  `job_description` text NOT NULL,
  `biodata_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_groups`
--

CREATE TABLE `hr_groups` (
  `id` tinyint(4) NOT NULL,
  `name_of_group` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_group_members`
--

CREATE TABLE `hr_group_members` (
  `id` bigint(20) NOT NULL,
  `group_id` tinyint(4) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_joined` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_group_messages`
--

CREATE TABLE `hr_group_messages` (
  `id` bigint(20) NOT NULL,
  `group_id` tinyint(4) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `file_upload_id` bigint(20) NOT NULL,
  `recipient_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_job_applicants`
--

CREATE TABLE `hr_job_applicants` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `primary_tel` varchar(45) NOT NULL,
  `secondary_tel` varchar(45) NOT NULL,
  `marital_status` enum('SINGLE','MARRIED','DIVORCED','WIDOWED') NOT NULL,
  `email` varchar(45) NOT NULL,
  `nationality` int(11) NOT NULL,
  `residence_address` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `application_status` enum('PENDING','SHORT-LISTED','DECLINED') NOT NULL,
  `curriculum_vitae` bigint(20) NOT NULL,
  `application_letter_id` bigint(20) NOT NULL,
  `job_vacancy_id` bigint(20) NOT NULL,
  `date_applied` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_job_interviews`
--

CREATE TABLE `hr_job_interviews` (
  `id` bigint(20) NOT NULL,
  `applicant_id` bigint(20) NOT NULL,
  `interview_date` date NOT NULL,
  `interview_status` enum('PENDING','SHORTLISTED','DECLINED') NOT NULL,
  `date_interviewed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_job_vacancies`
--

CREATE TABLE `hr_job_vacancies` (
  `id` bigint(20) NOT NULL,
  `job_title` varchar(50) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `contract_type` enum('FULL-TIME','PART-TIME','INTERN','CONTRACT') NOT NULL,
  `job_summary` text NOT NULL,
  `job_description` text NOT NULL,
  `qualification` text NOT NULL,
  `salary_expectation` varchar(45) NOT NULL,
  `slot` tinyint(4) NOT NULL,
  `deadline` date NOT NULL,
  `status` enum('ACTIVE','INACTIVE','CLOSED') NOT NULL,
  `posted_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_leave_policies`
--

CREATE TABLE `hr_leave_policies` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `leave_type` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_position_benefits`
--

CREATE TABLE `hr_position_benefits` (
  `id` tinyint(4) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `benefit_id` bigint(20) NOT NULL,
  `benefit_amount` double NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_position_category`
--

CREATE TABLE `hr_position_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `min_salary` double NOT NULL,
  `max_salary` double NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_position_category_benefits`
--

CREATE TABLE `hr_position_category_benefits` (
  `id` bigint(20) NOT NULL,
  `position_category` bigint(20) NOT NULL,
  `salary_type` enum('HOURLY','MONTHLY','COMISSIONED') NOT NULL,
  `benefit_id` bigint(20) NOT NULL,
  `benefit_amt` double NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_position_category_leave_policies`
--

CREATE TABLE `hr_position_category_leave_policies` (
  `id` bigint(20) NOT NULL,
  `position_category` bigint(20) NOT NULL,
  `policy_id` tinyint(4) NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_position_leave_policies`
--

CREATE TABLE `hr_position_leave_policies` (
  `id` tinyint(4) NOT NULL,
  `position_id` bigint(20) NOT NULL,
  `policy_id` tinyint(4) NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_postions`
--

CREATE TABLE `hr_postions` (
  `id` bigint(20) NOT NULL,
  `Parentid` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `position_category` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nominee_ratings`
--

CREATE TABLE `nominee_ratings` (
  `id` bigint(20) NOT NULL,
  `award_nominee_id` bigint(20) NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `rating` double NOT NULL,
  `rated_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets_issues`
--

CREATE TABLE `tickets_issues` (
  `id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `file_upload_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `priority` enum('LOW','NORMAL','HIGH','URGENT') NOT NULL,
  `status` enum('OPEN','RESOLVED','CANCELLED','ON-HOLD','IN-PROGRESS') NOT NULL,
  `issued_to` bigint(20) NOT NULL,
  `watchers` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets_projects`
--

CREATE TABLE `tickets_projects` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `team_members` varchar(255) NOT NULL,
  `status` enum('IN-PROGRESS','ON-HOLD','CANCELLED') NOT NULL,
  `re_opened_by` bigint(20) NOT NULL,
  `re_opened_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets_resolutions`
--

CREATE TABLE `tickets_resolutions` (
  `id` bigint(20) NOT NULL,
  `issue_id` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `file_upload_id` bigint(20) NOT NULL,
  `previous_status` varchar(45) NOT NULL,
  `status` enum('OPEN','RESOLVED','CANCELLED','ON-HOLD','IN-PROGRESS') NOT NULL,
  `previous_assignee` bigint(20) NOT NULL,
  `assigned_to` bigint(20) NOT NULL,
  `previous_priority` varchar(45) NOT NULL,
  `priority` enum('LOW','NORMAL','HIGH','URGENT') NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `financed_by` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `session` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `speakers` varchar(255) NOT NULL,
  `certificate_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `company_asset_1` bigint(20) NOT NULL,
  `departments_id` varchar(45) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_nominees`
--

CREATE TABLE `training_nominees` (
  `id` bigint(20) NOT NULL,
  `training_id` int(11) NOT NULL,
  `nominee_biodata_id` bigint(20) NOT NULL,
  `nominee_remarks` text NOT NULL,
  `nominee_performance` varchar(255) NOT NULL,
  `performance_rated_by` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_dashboard_items`
--
ALTER TABLE `access_dashboard_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access_password_reset_requests`
--
ALTER TABLE `access_password_reset_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_272` (`user_id`);

--
-- Indexes for table `access_roles_privileges_group`
--
ALTER TABLE `access_roles_privileges_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_288` (`created_by`),
  ADD KEY `fkIdx_292` (`position_id`);

--
-- Indexes for table `access_roles_privileges_user`
--
ALTER TABLE `access_roles_privileges_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_301` (`position_id`),
  ADD KEY `fkIdx_304` (`user_id`),
  ADD KEY `fkIdx_492` (`modified_by`);

--
-- Indexes for table `access_users`
--
ALTER TABLE `access_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_323` (`profile_pic_id`),
  ADD KEY `fkIdx_351` (`biodata_id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_928` (`file_upload_id`),
  ADD KEY `fkIdx_932` (`created_by`);

--
-- Indexes for table `award_nominees`
--
ALTER TABLE `award_nominees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1109` (`award_id`),
  ADD KEY `fkIdx_1113` (`nominee_id`),
  ADD KEY `fkIdx_1118` (`nominated_by`);

--
-- Indexes for table `award_winners`
--
ALTER TABLE `award_winners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_983` (`awarded_by`),
  ADD KEY `fkIdx_986` (`award_file_id`),
  ADD KEY `fkIdx_1126` (`nominee_rating_id`),
  ADD KEY `fkIdx_1131` (`confirmed_by`);

--
-- Indexes for table `blobs_upload`
--
ALTER TABLE `blobs_upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_400` (`user_id`);

--
-- Indexes for table `company_assets`
--
ALTER TABLE `company_assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1069` (`company_id`),
  ADD KEY `fkIdx_1073` (`asset_type_id`),
  ADD KEY `fkIdx_1079` (`acquired_by`),
  ADD KEY `fkIdx_1084` (`created_by`);

--
-- Indexes for table `company_assets_category`
--
ALTER TABLE `company_assets_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_assets_type`
--
ALTER TABLE `company_assets_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1064` (`asset_category_id`);

--
-- Indexes for table `company_awards_certificates_won`
--
ALTER TABLE `company_awards_certificates_won`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_772` (`company_id`),
  ADD KEY `fkIdx_776` (`received_by`),
  ADD KEY `fkIdx_779` (`file_upload_id`);

--
-- Indexes for table `company_award_rating_criteria`
--
ALTER TABLE `company_award_rating_criteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_958` (`created_by`);

--
-- Indexes for table `company_basic_details`
--
ALTER TABLE `company_basic_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_754` (`company_id`),
  ADD KEY `fkIdx_788` (`handbook_file`),
  ADD KEY `fkIdx_1350` (`created_by`);

--
-- Indexes for table `company_branches`
--
ALTER TABLE `company_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_597` (`branch_head`),
  ADD KEY `fkIdx_601` (`created_by`),
  ADD KEY `fkIdx_865` (`company_id`),
  ADD KEY `fkIdx_1229` (`Parentid`);

--
-- Indexes for table `company_branch_departments`
--
ALTER TABLE `company_branch_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1245` (`branch_id`),
  ADD KEY `fkIdx_1249` (`department_id`),
  ADD KEY `fkIdx_1255` (`created_by`);

--
-- Indexes for table `company_branch_department_heads`
--
ALTER TABLE `company_branch_department_heads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_185` (`hod_biodata_id`),
  ADD KEY `fkIdx_420` (`appointed_by`),
  ADD KEY `fkIdx_1258` (`department_id`);

--
-- Indexes for table `company_branch_positions`
--
ALTER TABLE `company_branch_positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1268` (`position_id`),
  ADD KEY `fkIdx_1273` (`department_id`),
  ADD KEY `fkIdx_1280` (`created_by`);

--
-- Indexes for table `company_business_details`
--
ALTER TABLE `company_business_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_784` (`logo`);

--
-- Indexes for table `company_clients_portfolio`
--
ALTER TABLE `company_clients_portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_759` (`company_id`);

--
-- Indexes for table `company_targets`
--
ALTER TABLE `company_targets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_822` (`created_by`),
  ADD KEY `fkIdx_825` (`rated_by`),
  ADD KEY `fkIdx_830` (`target_category_id`);

--
-- Indexes for table `company_target_categories`
--
ALTER TABLE `company_target_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_808` (`department_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_benefits`
--
ALTER TABLE `hr_benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_department_info`
--
ALTER TABLE `hr_department_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_417` (`created_by`),
  ADD KEY `fkIdx_1225` (`Parentid`);

--
-- Indexes for table `hr_employee_biodata`
--
ALTER TABLE `hr_employee_biodata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_471` (`curriculum_vitae`),
  ADD KEY `fkIdx_475` (`application_letter`);

--
-- Indexes for table `hr_employee_dependents`
--
ALTER TABLE `hr_employee_dependents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_467` (`biodata_id`);

--
-- Indexes for table `hr_employee_job_details`
--
ALTER TABLE `hr_employee_job_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_448` (`biodata_id`),
  ADD KEY `fkIdx_454` (`position_id`);

--
-- Indexes for table `hr_employee_leave_request`
--
ALTER TABLE `hr_employee_leave_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_901` (`employee_biodata_id`),
  ADD KEY `fkIdx_905` (`leave_policy_id`),
  ADD KEY `fkIdx_908` (`approver_biodata_id`);

--
-- Indexes for table `hr_employee_schools_attended`
--
ALTER TABLE `hr_employee_schools_attended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_852` (`biodata_id`);

--
-- Indexes for table `hr_employee_work_experience`
--
ALTER TABLE `hr_employee_work_experience`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_585` (`biodata_id`);

--
-- Indexes for table `hr_groups`
--
ALTER TABLE `hr_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1163` (`created_by`);

--
-- Indexes for table `hr_group_members`
--
ALTER TABLE `hr_group_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1172` (`group_id`),
  ADD KEY `fkIdx_1176` (`member_id`);

--
-- Indexes for table `hr_group_messages`
--
ALTER TABLE `hr_group_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1185` (`group_id`),
  ADD KEY `fkIdx_1189` (`sender_id`),
  ADD KEY `fkIdx_1193` (`recipient_id`),
  ADD KEY `fkIdx_1198` (`file_upload_id`);

--
-- Indexes for table `hr_job_applicants`
--
ALTER TABLE `hr_job_applicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_665` (`curriculum_vitae`),
  ADD KEY `fkIdx_668` (`application_letter_id`),
  ADD KEY `fkIdx_697` (`job_vacancy_id`),
  ADD KEY `fkIdx_1152` (`nationality`);

--
-- Indexes for table `hr_job_interviews`
--
ALTER TABLE `hr_job_interviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_687` (`applicant_id`);

--
-- Indexes for table `hr_job_vacancies`
--
ALTER TABLE `hr_job_vacancies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_646` (`position_id`),
  ADD KEY `fkIdx_649` (`branch_id`),
  ADD KEY `fkIdx_659` (`posted_by`);

--
-- Indexes for table `hr_leave_policies`
--
ALTER TABLE `hr_leave_policies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1334` (`created_by`);

--
-- Indexes for table `hr_position_benefits`
--
ALTER TABLE `hr_position_benefits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1297` (`created_by`),
  ADD KEY `fkIdx_1300` (`benefit_id`),
  ADD KEY `fkIdx_1304` (`position_id`);

--
-- Indexes for table `hr_position_category`
--
ALTER TABLE `hr_position_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1287` (`created_by`);

--
-- Indexes for table `hr_position_category_benefits`
--
ALTER TABLE `hr_position_category_benefits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_180` (`benefit_id`),
  ADD KEY `fkIdx_434` (`position_category`),
  ADD KEY `fkIdx_440` (`created_by`);

--
-- Indexes for table `hr_position_category_leave_policies`
--
ALTER TABLE `hr_position_category_leave_policies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_429` (`position_category`),
  ADD KEY `fkIdx_444` (`created_by`),
  ADD KEY `fkIdx_1316` (`policy_id`);

--
-- Indexes for table `hr_position_leave_policies`
--
ALTER TABLE `hr_position_leave_policies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1324` (`position_id`),
  ADD KEY `fkIdx_1328` (`policy_id`),
  ADD KEY `fkIdx_1340` (`created_by`);

--
-- Indexes for table `hr_postions`
--
ALTER TABLE `hr_postions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_397` (`created_by`),
  ADD KEY `fkIdx_404` (`position_category`),
  ADD KEY `fkIdx_869` (`Parentid`);

--
-- Indexes for table `nominee_ratings`
--
ALTER TABLE `nominee_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_961` (`criteria_id`),
  ADD KEY `fkIdx_965` (`rated_by`),
  ADD KEY `fkIdx_1121` (`award_nominee_id`);

--
-- Indexes for table `tickets_issues`
--
ALTER TABLE `tickets_issues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_509` (`created_by`),
  ADD KEY `fkIdx_512` (`issued_to`),
  ADD KEY `fkIdx_515` (`file_upload_id`),
  ADD KEY `fkIdx_532` (`project_id`);

--
-- Indexes for table `tickets_projects`
--
ALTER TABLE `tickets_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_526` (`created_by`),
  ADD KEY `fkIdx_529` (`re_opened_by`);

--
-- Indexes for table `tickets_resolutions`
--
ALTER TABLE `tickets_resolutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_546` (`issue_id`),
  ADD KEY `fkIdx_550` (`assigned_to`),
  ADD KEY `fkIdx_554` (`created_by`),
  ADD KEY `fkIdx_557` (`file_upload_id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1009` (`certificate_id`),
  ADD KEY `fkIdx_1354` (`created_by`);

--
-- Indexes for table `training_nominees`
--
ALTER TABLE `training_nominees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_1016` (`training_id`),
  ADD KEY `fkIdx_1021` (`nominee_biodata_id`),
  ADD KEY `fkIdx_1024` (`created_by`),
  ADD KEY `fkIdx_1093` (`performance_rated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_dashboard_items`
--
ALTER TABLE `access_dashboard_items`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `access_password_reset_requests`
--
ALTER TABLE `access_password_reset_requests`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `access_roles_privileges_group`
--
ALTER TABLE `access_roles_privileges_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `access_roles_privileges_user`
--
ALTER TABLE `access_roles_privileges_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `access_users`
--
ALTER TABLE `access_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `award_nominees`
--
ALTER TABLE `award_nominees`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `award_winners`
--
ALTER TABLE `award_winners`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blobs_upload`
--
ALTER TABLE `blobs_upload`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_assets`
--
ALTER TABLE `company_assets`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_assets_category`
--
ALTER TABLE `company_assets_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_assets_type`
--
ALTER TABLE `company_assets_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_awards_certificates_won`
--
ALTER TABLE `company_awards_certificates_won`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_award_rating_criteria`
--
ALTER TABLE `company_award_rating_criteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_basic_details`
--
ALTER TABLE `company_basic_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_branches`
--
ALTER TABLE `company_branches`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_branch_departments`
--
ALTER TABLE `company_branch_departments`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_branch_department_heads`
--
ALTER TABLE `company_branch_department_heads`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_branch_positions`
--
ALTER TABLE `company_branch_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_business_details`
--
ALTER TABLE `company_business_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_clients_portfolio`
--
ALTER TABLE `company_clients_portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_targets`
--
ALTER TABLE `company_targets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_target_categories`
--
ALTER TABLE `company_target_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_benefits`
--
ALTER TABLE `hr_benefits`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_department_info`
--
ALTER TABLE `hr_department_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_biodata`
--
ALTER TABLE `hr_employee_biodata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_dependents`
--
ALTER TABLE `hr_employee_dependents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_job_details`
--
ALTER TABLE `hr_employee_job_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_leave_request`
--
ALTER TABLE `hr_employee_leave_request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_schools_attended`
--
ALTER TABLE `hr_employee_schools_attended`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_employee_work_experience`
--
ALTER TABLE `hr_employee_work_experience`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_groups`
--
ALTER TABLE `hr_groups`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_group_members`
--
ALTER TABLE `hr_group_members`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_job_applicants`
--
ALTER TABLE `hr_job_applicants`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_job_interviews`
--
ALTER TABLE `hr_job_interviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_job_vacancies`
--
ALTER TABLE `hr_job_vacancies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_leave_policies`
--
ALTER TABLE `hr_leave_policies`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_position_benefits`
--
ALTER TABLE `hr_position_benefits`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_position_category`
--
ALTER TABLE `hr_position_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_position_category_benefits`
--
ALTER TABLE `hr_position_category_benefits`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_position_category_leave_policies`
--
ALTER TABLE `hr_position_category_leave_policies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_position_leave_policies`
--
ALTER TABLE `hr_position_leave_policies`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hr_postions`
--
ALTER TABLE `hr_postions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nominee_ratings`
--
ALTER TABLE `nominee_ratings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tickets_issues`
--
ALTER TABLE `tickets_issues`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tickets_projects`
--
ALTER TABLE `tickets_projects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tickets_resolutions`
--
ALTER TABLE `tickets_resolutions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_nominees`
--
ALTER TABLE `training_nominees`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access_password_reset_requests`
--
ALTER TABLE `access_password_reset_requests`
  ADD CONSTRAINT `requester_user_id` FOREIGN KEY (`user_id`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `access_roles_privileges_group`
--
ALTER TABLE `access_roles_privileges_group`
  ADD CONSTRAINT `group_permission_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `position_permissions` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`);

--
-- Constraints for table `access_roles_privileges_user`
--
ALTER TABLE `access_roles_privileges_user`
  ADD CONSTRAINT `permission_modified_by` FOREIGN KEY (`modified_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `position_permission_assignment` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`),
  ADD CONSTRAINT `user_assigned_permission` FOREIGN KEY (`user_id`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `access_users`
--
ALTER TABLE `access_users`
  ADD CONSTRAINT `user_biodata_link` FOREIGN KEY (`biodata_id`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `user_profile_pic_file` FOREIGN KEY (`profile_pic_id`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `awards`
--
ALTER TABLE `awards`
  ADD CONSTRAINT `FK_928` FOREIGN KEY (`file_upload_id`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `FK_932` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `award_nominees`
--
ALTER TABLE `award_nominees`
  ADD CONSTRAINT `FK_1109` FOREIGN KEY (`award_id`) REFERENCES `awards` (`id`),
  ADD CONSTRAINT `FK_1113` FOREIGN KEY (`nominee_id`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_1118` FOREIGN KEY (`nominated_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `award_winners`
--
ALTER TABLE `award_winners`
  ADD CONSTRAINT `FK_1126` FOREIGN KEY (`nominee_rating_id`) REFERENCES `nominee_ratings` (`id`),
  ADD CONSTRAINT `FK_1131` FOREIGN KEY (`confirmed_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_983` FOREIGN KEY (`awarded_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_986` FOREIGN KEY (`award_file_id`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `blobs_upload`
--
ALTER TABLE `blobs_upload`
  ADD CONSTRAINT `uploader_id` FOREIGN KEY (`user_id`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `company_assets`
--
ALTER TABLE `company_assets`
  ADD CONSTRAINT `FK_1069` FOREIGN KEY (`company_id`) REFERENCES `company_business_details` (`id`),
  ADD CONSTRAINT `FK_1073` FOREIGN KEY (`asset_type_id`) REFERENCES `company_assets_type` (`id`),
  ADD CONSTRAINT `FK_1079` FOREIGN KEY (`acquired_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_1084` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `company_assets_type`
--
ALTER TABLE `company_assets_type`
  ADD CONSTRAINT `FK_1064` FOREIGN KEY (`asset_category_id`) REFERENCES `company_assets_category` (`id`);

--
-- Constraints for table `company_awards_certificates_won`
--
ALTER TABLE `company_awards_certificates_won`
  ADD CONSTRAINT `FK_772` FOREIGN KEY (`company_id`) REFERENCES `company_business_details` (`id`),
  ADD CONSTRAINT `FK_776` FOREIGN KEY (`received_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_779` FOREIGN KEY (`file_upload_id`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `company_award_rating_criteria`
--
ALTER TABLE `company_award_rating_criteria`
  ADD CONSTRAINT `FK_958` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `company_basic_details`
--
ALTER TABLE `company_basic_details`
  ADD CONSTRAINT `FK_1350` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_754` FOREIGN KEY (`company_id`) REFERENCES `company_business_details` (`id`),
  ADD CONSTRAINT `FK_788` FOREIGN KEY (`handbook_file`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `company_branches`
--
ALTER TABLE `company_branches`
  ADD CONSTRAINT `Branch_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_1229` FOREIGN KEY (`Parentid`) REFERENCES `company_branches` (`id`),
  ADD CONSTRAINT `FK_865` FOREIGN KEY (`company_id`) REFERENCES `company_business_details` (`id`),
  ADD CONSTRAINT `company_branch_head` FOREIGN KEY (`branch_head`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `company_branch_departments`
--
ALTER TABLE `company_branch_departments`
  ADD CONSTRAINT `FK_1245` FOREIGN KEY (`branch_id`) REFERENCES `company_branches` (`id`),
  ADD CONSTRAINT `FK_1249` FOREIGN KEY (`department_id`) REFERENCES `hr_department_info` (`id`),
  ADD CONSTRAINT `FK_1255` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `company_branch_department_heads`
--
ALTER TABLE `company_branch_department_heads`
  ADD CONSTRAINT `FK_1258` FOREIGN KEY (`department_id`) REFERENCES `company_branch_departments` (`id`),
  ADD CONSTRAINT `hod_appointor` FOREIGN KEY (`appointed_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `hod_user_id` FOREIGN KEY (`hod_biodata_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `company_branch_positions`
--
ALTER TABLE `company_branch_positions`
  ADD CONSTRAINT `FK_1268` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`),
  ADD CONSTRAINT `FK_1273` FOREIGN KEY (`department_id`) REFERENCES `company_branch_departments` (`id`),
  ADD CONSTRAINT `FK_1280` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `company_business_details`
--
ALTER TABLE `company_business_details`
  ADD CONSTRAINT `FK_784` FOREIGN KEY (`logo`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `company_clients_portfolio`
--
ALTER TABLE `company_clients_portfolio`
  ADD CONSTRAINT `FK_759` FOREIGN KEY (`company_id`) REFERENCES `company_business_details` (`id`);

--
-- Constraints for table `company_targets`
--
ALTER TABLE `company_targets`
  ADD CONSTRAINT `FK_822` FOREIGN KEY (`created_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_825` FOREIGN KEY (`rated_by`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_830` FOREIGN KEY (`target_category_id`) REFERENCES `company_target_categories` (`id`);

--
-- Constraints for table `company_target_categories`
--
ALTER TABLE `company_target_categories`
  ADD CONSTRAINT `FK_808` FOREIGN KEY (`department_id`) REFERENCES `hr_department_info` (`id`);

--
-- Constraints for table `hr_department_info`
--
ALTER TABLE `hr_department_info`
  ADD CONSTRAINT `FK_1225` FOREIGN KEY (`Parentid`) REFERENCES `hr_department_info` (`id`),
  ADD CONSTRAINT `department_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_employee_biodata`
--
ALTER TABLE `hr_employee_biodata`
  ADD CONSTRAINT `employee_application_letter` FOREIGN KEY (`application_letter`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `employee_cv` FOREIGN KEY (`curriculum_vitae`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `hr_employee_dependents`
--
ALTER TABLE `hr_employee_dependents`
  ADD CONSTRAINT `employee_emergency_contact` FOREIGN KEY (`biodata_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `hr_employee_job_details`
--
ALTER TABLE `hr_employee_job_details`
  ADD CONSTRAINT `biodata_employee_link` FOREIGN KEY (`biodata_id`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `employees_position` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`);

--
-- Constraints for table `hr_employee_leave_request`
--
ALTER TABLE `hr_employee_leave_request`
  ADD CONSTRAINT `FK_901` FOREIGN KEY (`employee_biodata_id`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_905` FOREIGN KEY (`leave_policy_id`) REFERENCES `hr_position_category_leave_policies` (`id`),
  ADD CONSTRAINT `FK_908` FOREIGN KEY (`approver_biodata_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `hr_employee_schools_attended`
--
ALTER TABLE `hr_employee_schools_attended`
  ADD CONSTRAINT `FK_852` FOREIGN KEY (`biodata_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `hr_employee_work_experience`
--
ALTER TABLE `hr_employee_work_experience`
  ADD CONSTRAINT `employee_employement_history` FOREIGN KEY (`biodata_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `hr_groups`
--
ALTER TABLE `hr_groups`
  ADD CONSTRAINT `FK_1163` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_group_members`
--
ALTER TABLE `hr_group_members`
  ADD CONSTRAINT `FK_1172` FOREIGN KEY (`group_id`) REFERENCES `hr_groups` (`id`),
  ADD CONSTRAINT `FK_1176` FOREIGN KEY (`member_id`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `hr_group_messages`
--
ALTER TABLE `hr_group_messages`
  ADD CONSTRAINT `FK_1185` FOREIGN KEY (`group_id`) REFERENCES `hr_groups` (`id`),
  ADD CONSTRAINT `FK_1189` FOREIGN KEY (`sender_id`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_1193` FOREIGN KEY (`recipient_id`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_1198` FOREIGN KEY (`file_upload_id`) REFERENCES `blobs_upload` (`id`);

--
-- Constraints for table `hr_job_applicants`
--
ALTER TABLE `hr_job_applicants`
  ADD CONSTRAINT `FK_1152` FOREIGN KEY (`nationality`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `FK_665` FOREIGN KEY (`curriculum_vitae`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `FK_668` FOREIGN KEY (`application_letter_id`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `FK_697` FOREIGN KEY (`job_vacancy_id`) REFERENCES `hr_job_vacancies` (`id`);

--
-- Constraints for table `hr_job_interviews`
--
ALTER TABLE `hr_job_interviews`
  ADD CONSTRAINT `FK_687` FOREIGN KEY (`applicant_id`) REFERENCES `hr_job_applicants` (`id`);

--
-- Constraints for table `hr_job_vacancies`
--
ALTER TABLE `hr_job_vacancies`
  ADD CONSTRAINT `FK_646` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`),
  ADD CONSTRAINT `FK_649` FOREIGN KEY (`branch_id`) REFERENCES `company_branches` (`id`),
  ADD CONSTRAINT `FK_659` FOREIGN KEY (`posted_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_leave_policies`
--
ALTER TABLE `hr_leave_policies`
  ADD CONSTRAINT `FK_1334` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_position_benefits`
--
ALTER TABLE `hr_position_benefits`
  ADD CONSTRAINT `FK_1297` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_1300` FOREIGN KEY (`benefit_id`) REFERENCES `hr_benefits` (`id`),
  ADD CONSTRAINT `FK_1304` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`);

--
-- Constraints for table `hr_position_category`
--
ALTER TABLE `hr_position_category`
  ADD CONSTRAINT `FK_1287` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_position_category_benefits`
--
ALTER TABLE `hr_position_category_benefits`
  ADD CONSTRAINT `position_category_benefit_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `position_category_benefits` FOREIGN KEY (`position_category`) REFERENCES `hr_position_category` (`id`),
  ADD CONSTRAINT `positions_benefit_type` FOREIGN KEY (`benefit_id`) REFERENCES `hr_benefits` (`id`);

--
-- Constraints for table `hr_position_category_leave_policies`
--
ALTER TABLE `hr_position_category_leave_policies`
  ADD CONSTRAINT `FK_1316` FOREIGN KEY (`policy_id`) REFERENCES `hr_leave_policies` (`id`),
  ADD CONSTRAINT `leaves_created_by` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `position_category_leaves` FOREIGN KEY (`position_category`) REFERENCES `hr_position_category` (`id`);

--
-- Constraints for table `hr_position_leave_policies`
--
ALTER TABLE `hr_position_leave_policies`
  ADD CONSTRAINT `FK_1324` FOREIGN KEY (`position_id`) REFERENCES `hr_postions` (`id`),
  ADD CONSTRAINT `FK_1328` FOREIGN KEY (`policy_id`) REFERENCES `hr_leave_policies` (`id`),
  ADD CONSTRAINT `FK_1340` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `hr_postions`
--
ALTER TABLE `hr_postions`
  ADD CONSTRAINT `FK_869` FOREIGN KEY (`Parentid`) REFERENCES `hr_postions` (`id`),
  ADD CONSTRAINT `position_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `positions_category` FOREIGN KEY (`position_category`) REFERENCES `hr_position_category` (`id`);

--
-- Constraints for table `nominee_ratings`
--
ALTER TABLE `nominee_ratings`
  ADD CONSTRAINT `FK_1121` FOREIGN KEY (`award_nominee_id`) REFERENCES `award_nominees` (`id`),
  ADD CONSTRAINT `FK_961` FOREIGN KEY (`criteria_id`) REFERENCES `company_award_rating_criteria` (`id`),
  ADD CONSTRAINT `FK_965` FOREIGN KEY (`rated_by`) REFERENCES `hr_employee_biodata` (`id`);

--
-- Constraints for table `tickets_issues`
--
ALTER TABLE `tickets_issues`
  ADD CONSTRAINT `creator_of_ticket` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `ticket_file_uploaded` FOREIGN KEY (`file_upload_id`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `ticket_issued_to` FOREIGN KEY (`issued_to`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `ticket_project_id` FOREIGN KEY (`project_id`) REFERENCES `tickets_projects` (`id`);

--
-- Constraints for table `tickets_projects`
--
ALTER TABLE `tickets_projects`
  ADD CONSTRAINT `project_creator` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `ticket_re-opener` FOREIGN KEY (`re_opened_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `tickets_resolutions`
--
ALTER TABLE `tickets_resolutions`
  ADD CONSTRAINT `ticket_issue_id` FOREIGN KEY (`issue_id`) REFERENCES `tickets_issues` (`id`),
  ADD CONSTRAINT `ticket_resolution_file_upload` FOREIGN KEY (`file_upload_id`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `tickets_assigned_to` FOREIGN KEY (`assigned_to`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `tickets_resolution_created_by` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `FK_1009` FOREIGN KEY (`certificate_id`) REFERENCES `blobs_upload` (`id`),
  ADD CONSTRAINT `FK_1354` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`);

--
-- Constraints for table `training_nominees`
--
ALTER TABLE `training_nominees`
  ADD CONSTRAINT `FK_1016` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`),
  ADD CONSTRAINT `FK_1021` FOREIGN KEY (`nominee_biodata_id`) REFERENCES `hr_employee_biodata` (`id`),
  ADD CONSTRAINT `FK_1024` FOREIGN KEY (`created_by`) REFERENCES `access_users` (`id`),
  ADD CONSTRAINT `FK_1093` FOREIGN KEY (`performance_rated_by`) REFERENCES `hr_employee_biodata` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
